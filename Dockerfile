FROM java:8
COPY /book-0.0.1-SNAPSHOT.jar /book-0.0.1-SNAPSHOT.jar
EXPOSE 8090
ENTRYPOINT java -jar /book-0.0.1-SNAPSHOT.jar